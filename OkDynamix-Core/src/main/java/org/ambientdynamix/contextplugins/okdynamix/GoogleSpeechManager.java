package org.ambientdynamix.contextplugins.okdynamix;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.util.Log;

/**
 * Created by shivam on 8/26/16.
 */
public class GoogleSpeechManager {
    private String TAG = "GoogleSpeechManager";
    private final Context mContext;
    private android.speech.SpeechRecognizer mGoogleSpeechRecognizer;
    private android.speech.RecognitionListener mRecognitionListener;

    public GoogleSpeechManager(Context mContext, android.speech.RecognitionListener mRecognitionListener) {
        this.mContext = mContext;
        this.mRecognitionListener = mRecognitionListener;
    }

    public boolean listen() {
        Log.i(TAG, "Starting to listen");
        Handler mainHandler = new Handler(mContext.getMainLooper());

        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                try {
                    mGoogleSpeechRecognizer = android.speech.SpeechRecognizer
                            .createSpeechRecognizer(mContext);
                    mGoogleSpeechRecognizer.setRecognitionListener(mRecognitionListener);
                    Intent mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                    mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                    mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_CONFIDENCE_SCORES, true);
                    mGoogleSpeechRecognizer.startListening(mSpeechRecognizerIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        mainHandler.post(myRunnable);
        return true;
    }

    public boolean destroy() {
        if (mGoogleSpeechRecognizer != null) {
            mGoogleSpeechRecognizer.cancel();
            mGoogleSpeechRecognizer.destroy();
            mGoogleSpeechRecognizer = null;
        }
        return true;
    }
}
