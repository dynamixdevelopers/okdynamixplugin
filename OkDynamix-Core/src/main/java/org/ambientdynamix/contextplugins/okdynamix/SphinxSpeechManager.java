package org.ambientdynamix.contextplugins.okdynamix;

import android.content.Context;
import android.util.Log;
import edu.cmu.pocketsphinx.Assets;
import edu.cmu.pocketsphinx.Hypothesis;
import edu.cmu.pocketsphinx.RecognitionListener;
import edu.cmu.pocketsphinx.SpeechRecognizerSetup;

import java.io.File;

import android.os.Handler;

/**
 * Created by shivam on 8/26/16.
 */
public class SphinxSpeechManager {

    private final String SEARCH_NAME = "dynamix_hotword";
    private Context mContext;
    private String path;
    edu.cmu.pocketsphinx.SpeechRecognizer mPocketSphinxRecognizer;
    edu.cmu.pocketsphinx.RecognitionListener mRecognitionlistener;
    private String TAG = "SphinxSpeechManager";

    public SphinxSpeechManager(Context mContext, String path, RecognitionListener mRecognitionlistener) {
        this.path = path;
        this.mRecognitionlistener = mRecognitionlistener;
        this.mContext = mContext;
    }

    public boolean init() {
        try {
            final SpeechRecognizerSetup speechRecognizerSetup = SpeechRecognizerSetup.defaultSetup();
            final File assetsDir = new File(path);
            Log.i(TAG, path);
            speechRecognizerSetup.setAcousticModel(new File(assetsDir, "en-us-ptm"));
            speechRecognizerSetup.setDictionary(new File(assetsDir, "cmudict-en-us.dict"));
            speechRecognizerSetup.setKeywordThreshold(1e-45f);
            mPocketSphinxRecognizer = speechRecognizerSetup.getRecognizer();
            mPocketSphinxRecognizer.addListener(mRecognitionlistener);
            mPocketSphinxRecognizer.addKeyphraseSearch(SEARCH_NAME, OkDynamicsManager.HOTWORD);
            listen();
            Log.i(TAG, "Successfully init Sphinx Speech Manager");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public void listen() {
        Log.d(TAG, "Starting to listen");
        mPocketSphinxRecognizer.startListening(SEARCH_NAME);
    }

    public void stopListening() {
        mPocketSphinxRecognizer.stop();
    }
}
