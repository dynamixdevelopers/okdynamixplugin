package org.ambientdynamix.contextplugins.okdynamix;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import edu.cmu.pocketsphinx.Hypothesis;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shivam on 8/26/16.
 */
public class OkDynamicsManager {

    private final String TAG = "OkDynamicsManager";
    public static final String HOTWORD = "ok dynamics";
    private final String pathToSphinxAssets;
    private ArrayList<OkDynamicsListener> okDynamicsListeners;
    private Context mContext;
    SphinxSpeechManager mSphinxSpeechManager;
    GoogleSpeechManager mGoogleSpeechManager;

    public OkDynamicsManager(Context mContext, String pathToSphinxAssets) {
        this.mContext = mContext;
        this.pathToSphinxAssets = pathToSphinxAssets;
        okDynamicsListeners = new ArrayList<>();
    }

    public void addListener(OkDynamicsListener listener) {
        okDynamicsListeners.add(listener);
    }

    public void listen() {
        if (mSphinxSpeechManager == null) {
            mSphinxSpeechManager = new SphinxSpeechManager(mContext, this.pathToSphinxAssets, new MySphinxRecognitionListener());
            mSphinxSpeechManager.init();
        }
        if (mGoogleSpeechManager == null) {
            mGoogleSpeechManager = new GoogleSpeechManager(mContext, new MyGoogleRecognitionListener());
        }
    }

    public interface OkDynamicsListener {
        void onHotwordDetected();

        void onGoogleResults(VoiceResults2 results);
    }

    private class MySphinxRecognitionListener implements edu.cmu.pocketsphinx.RecognitionListener {

        @Override
        public void onBeginningOfSpeech() {
            Log.i(TAG, "onBeginningOfSpeech");

        }

        @Override
        public void onEndOfSpeech() {
            Log.i(TAG, "onEndOfSpeech");
        }

        @Override
        public void onPartialResult(Hypothesis hypothesis) {
            if (hypothesis != null) {
                String text = hypothesis.getHypstr();
                Log.i(TAG, "Hotword detected :" + text);
                if (text.equals(HOTWORD)) {
                    mSphinxSpeechManager.stopListening();
                    for (OkDynamicsListener listener : okDynamicsListeners) {
                        listener.onHotwordDetected();
                    }
                    mGoogleSpeechManager.listen();
                }
            }
        }

        @Override
        public void onResult(Hypothesis hypothesis) {

        }

        @Override
        public void onError(Exception e) {
            Log.e(TAG, "onError :" + e.toString());

        }

        @Override
        public void onTimeout() {
            Log.e(TAG, "onTimeout");
        }
    }

    private class MyGoogleRecognitionListener implements android.speech.RecognitionListener {

        @Override
        public void onReadyForSpeech(Bundle params) {

        }

        @Override
        public void onBeginningOfSpeech() {

        }

        @Override
        public void onRmsChanged(float rmsdB) {

        }

        @Override
        public void onBufferReceived(byte[] buffer) {

        }

        @Override
        public void onEndOfSpeech() {

        }

        @Override
        public void onError(int error) {

        }

        @Override
        public void onResults(Bundle results) {
            if ((results != null)
                    && results
                    .containsKey(android.speech.SpeechRecognizer.RESULTS_RECOGNITION)) {
                List<String> tmp = results.getStringArrayList(android.speech.SpeechRecognizer.RESULTS_RECOGNITION);
                String[] voiceArray = tmp.toArray(new String[tmp.size()]);
                float[] scoreArray = results.getFloatArray("confidence_scores");
                List<IVoiceResult> voiceResults = new ArrayList<>();
                for (int i = 0; i < voiceArray.length; i++) {
                    if (scoreArray != null && scoreArray.length > 0)
                        voiceResults.add(new VoiceResult(voiceArray[i], scoreArray[i]));
                    else
                        voiceResults.add(new VoiceResult(voiceArray[i], 0));
                }
                for (OkDynamicsListener listener : okDynamicsListeners) {
                    listener.onGoogleResults(new VoiceResults2(voiceResults));
                }
            }
            mGoogleSpeechManager.destroy();
            mSphinxSpeechManager.listen();
        }

        @Override
        public void onPartialResults(Bundle partialResults) {

        }

        @Override
        public void onEvent(int eventType, Bundle params) {

        }
    }
}
