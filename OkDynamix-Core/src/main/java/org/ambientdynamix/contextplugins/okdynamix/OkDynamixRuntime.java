/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.okdynamix;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import edu.cmu.pocketsphinx.Hypothesis;
import edu.cmu.pocketsphinx.RecognitionListener;
import org.ambientdynamix.api.contextplugin.ContextListenerInformation;
import org.ambientdynamix.api.contextplugin.ContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.ContextPluginSettings;
import org.ambientdynamix.api.contextplugin.PowerScheme;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/***
 * @author Shivam Verma
 */
public class OkDynamixRuntime extends ContextPluginRuntime {
    private final String TAG = "OkDynamixRuntime";
    private Context context;
    private String privateDataDir;
    private OkDynamicsManager okDynamicsManager;

    /**
     * Called once when the ContextPluginRuntime is first initialized. The implementing subclass should acquire the
     * resources necessary to run. If initialization is unsuccessful, the plug-ins should throw an exception and release
     * any acquired resources.
     */
    @Override
    public void init(PowerScheme powerScheme, ContextPluginSettings settings) throws Exception {
        // Set the power scheme
        this.setPowerScheme(powerScheme);
        // Store our secure context
        this.context = this.getSecuredContext();
        privateDataDir = getPrivateDataDirectory() + "/";
        okDynamicsManager = new OkDynamicsManager(context, privateDataDir + "assets/sync/");
        AssetsManager assetsManager = new AssetsManager();
        try {
            assetsManager.copyAssets(privateDataDir);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void start() {

    }

    /**
     * Called by the Dynamix Context Manager to stop context sensing or acting operations; however, any acquired
     * resources should be maintained, since start may be called again.
     */
    @Override
    public void stop() {

    }

    /**
     * Stops the runtime (if necessary) and then releases all acquired resources in preparation for garbage collection.
     * Once this method has been called, it may not be re-started and will be reclaimed by garbage collection sometime
     * in the indefinite future.
     */
    @Override
    public void destroy() {
        this.stop();

    }

    @Override
    public void handleContextRequest(final UUID requestId, String contextType) {

        switch (contextType) {
            case VoiceResults.CONTEXT_TYPE:

                GoogleSpeechManager googleSpeechManager = new GoogleSpeechManager(context, new android.speech.RecognitionListener() {
                    @Override
                    public void onReadyForSpeech(Bundle params) {

                    }

                    @Override
                    public void onBeginningOfSpeech() {

                    }

                    @Override
                    public void onRmsChanged(float rmsdB) {

                    }

                    @Override
                    public void onBufferReceived(byte[] buffer) {

                    }

                    @Override
                    public void onEndOfSpeech() {

                    }

                    @Override
                    public void onError(int error) {

                    }

                    @Override
                    public void onResults(Bundle results) {
                        if ((results != null)
                                && results
                                .containsKey(android.speech.SpeechRecognizer.RESULTS_RECOGNITION)) {
                            List<String> tmp = results.getStringArrayList(android.speech.SpeechRecognizer.RESULTS_RECOGNITION);
                            String[] voiceArray = tmp.toArray(new String[tmp.size()]);
                            float[] scoreArray = results.getFloatArray("confidence_scores");
                            List<IVoiceResult> voiceResults = new ArrayList<>();
                            for (int i = 0; i < voiceArray.length; i++) {
                                if (scoreArray != null && scoreArray.length > 0)
                                    voiceResults.add(new VoiceResult(voiceArray[i], scoreArray[i]));
                                else
                                    voiceResults.add(new VoiceResult(voiceArray[i], 0));
                            }
                            sendContextEvent(requestId, new VoiceResults(voiceResults));
                        }
                    }

                    @Override
                    public void onPartialResults(Bundle partialResults) {

                    }

                    @Override
                    public void onEvent(int eventType, Bundle params) {

                    }
                });

                googleSpeechManager.listen();
        }

    }

    @Override
    public void handleConfiguredContextRequest(UUID requestId, String contextType, Bundle config) {

    }

    @Override
    public void updateSettings(ContextPluginSettings settings) {
        // Not supported
    }

    @Override
    public void setPowerScheme(PowerScheme scheme) {
        // Not supported
    }

    @Override
    public boolean addContextlistener(final ContextListenerInformation listenerInfo) {
        if (listenerInfo.getContextType().equals(VoiceResults2.CONTEXT_TYPE)) {
            okDynamicsManager.addListener(new OkDynamicsManager.OkDynamicsListener() {
                @Override
                public void onHotwordDetected() {
                    Log.d(TAG, "Hotword Detected");
                }

                @Override
                public void onGoogleResults(VoiceResults2 results) {
                    Log.d(TAG, results.getStringRepresentation("text/plain"));
                    sendBroadcastContextEvent(results);
                }
            });
            okDynamicsManager.listen();
        }
        return true;
    }
}