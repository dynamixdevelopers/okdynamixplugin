package org.ambientdynamix.contextplugins.okdynamix;

import android.util.Log;

import java.io.*;

/**
 * Created by shivam on 8/26/16.
 */
public class AssetsManager {
    private static String TAG = "AssetsManager";

    private String[] relativeFilePaths = new String[]{
            "en-us-ptm/feat.params",
            "en-us-ptm/feat.params.md5",
            "en-us-ptm/mdef",
            "en-us-ptm/mdef.md5",
            "en-us-ptm/means",
            "en-us-ptm/means.md5",
            "en-us-ptm/noisedict",
            "en-us-ptm/noisedict.md5",
            "en-us-ptm/sendump",
            "en-us-ptm/sendump.md5",
            "en-us-ptm/transition_matrices",
            "en-us-ptm/transition_matrices.md5",
            "en-us-ptm/variances",
            "en-us-ptm/variances.md5",
            "assets.lst",
            "cmudict-en-us.dict",
            "cmudict-en-us.dict.md5",
            "digits.gram",
            "digits.gram.md5"
    };

    public void copyAssets(String destinationDirectory) throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        for (String path : relativeFilePaths) {
            path = "assets/sync/" + path;
            Log.d(TAG, "Trying : " + path);
            File destination = new File(destinationDirectory + path);
            File directory = destination.getParentFile();
            if (directory != null && !directory.exists() && !directory.mkdirs()) {
                Log.d(TAG, "Cannot create dir " + directory.getAbsolutePath());
            }

            InputStream in = classLoader.getResourceAsStream(path);
            OutputStream out = new FileOutputStream(destination);

            // Copy the bits from instream to outstream
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        }
    }
}
