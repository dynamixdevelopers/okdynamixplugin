/*
 * Copyright (C) the Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.okdynamix;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * A single result from Android's SpeechRecognizer. Contains the detected speech as text and an associated confidence score.
 * 
 * @author Darren Carlson
 * 
 */
class VoiceResult implements IVoiceResult, Parcelable {
	/**
	 * Required CREATOR field that generates instances of this Parcelable class from a Parcel.
	 * 
	 * @see http://developer.android.com/reference/android/os/Parcelable.Creator.html
	 */
	public static Parcelable.Creator<VoiceResult> CREATOR = new Parcelable.Creator<VoiceResult>() {
		public VoiceResult createFromParcel(Parcel in) {
			return new VoiceResult(in);
		}

		public VoiceResult[] newArray(int size) {
			return new VoiceResult[size];
		}
	};
	private String voiceResult;
	private float confidenceScore;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getVoiceResult() {
		return voiceResult;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public float getConfidenceScore() {
		return confidenceScore;
	}

	public VoiceResult(String result, float confidenceScore) {
		this.voiceResult = result;
		this.confidenceScore = confidenceScore;
	}

	protected void addResult(String result, float confidenceScore) {
		this.voiceResult = result;
		this.confidenceScore = confidenceScore;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName();
	};

//	/**
//	 * {@inheritDoc}
//	 */
//	@Override
//	public String getContextType() {
//		return "org.ambientdynamix.contextplugins.speechtotext.voiceresults";
//	}
//
//	/**
//	 * {@inheritDoc}
//	 */
//	@Override
//	public String getStringRepresentation(String format) {
//		if (format.equalsIgnoreCase("text/plain")) {
//			return voiceResult + "," + confidenceScore;
//		}
//		return null;
//	}
//
//	/**
//	 * {@inheritDoc}
//	 */
//	@Override
//	public String getImplementingClassname() {
//		return this.getClass().getName();
//	}
//
//	/**
//	 * {@inheritDoc}
//	 */
//	@Override
//	public Set<String> getStringRepresentationFormats() {
//		Set<String> formats = new HashSet<String>();
//		formats.add("text/plain");
//		return formats;
//	};

	private VoiceResult(final Parcel in) {
		this.voiceResult = in.readString();
		this.confidenceScore = in.readFloat();
	}

	public int describeContents() {
		return 0;
	}

	public void writeToParcel(Parcel out, int flags) {
		out.writeString(voiceResult);
		out.writeFloat(confidenceScore);
	}
}
