/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.okdynamix;

import org.ambientdynamix.api.application.IContextInfo;

/**
 * Interface representing a speech recognition result.
 * @author Darren Carlson
 *
 */
public interface IVoiceResult {
	/**
	 * Returns the voice result as a string.
	 */
	public abstract String getVoiceResult();

	/**
	 * Returns the confidence score associated with the voice result.
	 */
	public abstract float getConfidenceScore();
}