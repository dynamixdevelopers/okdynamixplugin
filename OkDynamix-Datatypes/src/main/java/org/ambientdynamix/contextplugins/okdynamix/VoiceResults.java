/*
 * Copyright (C) the Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.okdynamix;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.ambientdynamix.api.application.IContextInfo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * A complete result from Android's SpeechRecognizer, which contains each detected voiceResult for the individual scan.
 *
 * @author Darren Carlson
 */
class VoiceResults implements IContextInfo, IVoiceResults {
    /**
     * Required CREATOR field that generates instances of this Parcelable class from a Parcel.
     *
     * @see http://developer.android.com/reference/android/os/Parcelable.Creator.html
     */
    public static Parcelable.Creator<VoiceResults> CREATOR = new Parcelable.Creator<VoiceResults>() {
        public VoiceResults createFromParcel(Parcel in) {
            return new VoiceResults(in);
        }

        public VoiceResults[] newArray(int size) {
            return new VoiceResults[size];
        }
    };
    private List<IVoiceResult> voiceResults = new ArrayList<IVoiceResult>();
    public static final String CONTEXT_TYPE = "org.ambientdynamix.contextplugins.okdynamix.voiceresults";

    /*
     * (non-Javadoc)
     * @see org.ambientdynamix.contextplugins.okdynamix.IVoiceResults#getVoiceResults()
     */
    @Override
    public List<IVoiceResult> getVoiceResults() {
        return voiceResults;
    }

    protected void addResult(VoiceResult result) {
        this.voiceResults.add(result);
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

    ;

    /**
     * {@inheritDoc}
     */
    @Override
    public String getContextType() {
        return CONTEXT_TYPE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getStringRepresentation(String format) {
        if (format.equalsIgnoreCase("text/plain")) {
            StringBuilder results = new StringBuilder();
            for (IVoiceResult result : voiceResults) {
                results.append(result.getVoiceResult() + "|");
            }
            return results.toString();
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getImplementingClassname() {
        return this.getClass().getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<String> getStringRepresentationFormats() {
        Set<String> formats = new HashSet<String>();
        formats.add("text/plain");
        return formats;
    }

    ;

    public VoiceResults() {
    }

    public VoiceResults(List<IVoiceResult> voiceResults) {
        this.voiceResults = voiceResults;
    }

    private VoiceResults(final Parcel in) {
        in.readList(voiceResults, getClass().getClassLoader());
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeList(voiceResults);
    }
}
